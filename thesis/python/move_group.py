import bpy
import math
from mathutils import Vector
import mathutils

def translate_group_in_xy(group, translation):
    for ob in group.objects:
        ob.location += Vector(translation)

def duplicate_font(src_object):
    new_obj = src_object.copy()
    new_obj.data = src_object.data.copy()
    new_obj.animation_data_clear()
    bpy.context.scene.objects.link(new_obj)
    return new_obj

def duplicate_mesh(src_object):
    new_obj = src_object.copy()
    new_obj.data = src_object.data.copy()
    bpy.context.scene.objects.link(new_obj)
    return new_obj

def duplicate_and_move_group(group, translation=(0, 0, 0)):
    obs = [o for o in group.objects]
    new_group = bpy.data.groups.new("dh_group")
    print("Input Group = {}".format(group.name))
    print("Number of objects in the created new group({}) = {}".format(new_group.name, len(new_group.objects)))

    for ob in obs:
        print("Operating on object {} with type {}".format(ob.name, ob.type))
        if(ob.type == 'MESH'):
            new_obj = duplicate_font(ob)
            bpy.context.scene.objects.active = new_obj

        if(ob.type == 'FONT'):
            new_obj = duplicate_font(ob)
            bpy.context.scene.objects.active = new_obj

        print("Current active object after duplication = {}".format(bpy.context.active_object.name))
        bpy.context.active_object.location += Vector(translation)
        print("Current Number of objects (before linking) in the created new group({}) = {}".format(new_group.name, len(new_group.objects)))
        new_group.objects.link(bpy.context.active_object)
        print("Current Number of objects (after linking) in the created new group({}) = {}".format(new_group.name, len(new_group.objects)))
        bpy.ops.object.select_all(action='DESELECT')

    # bpy.ops.object.duplicate(linked=False, mode='TRANSLATION')
    # print("Current active object after duplication = {}".format(bpy.context.active_object.name))

    return new_group


# rotation about the z-axis, angle in degrees
def rotate_group_about_z(group, rotation=0.0):
    obs = [o for o in group.objects]
    print("Number of objects in the Input Group({}) = {}".format(group.name, len(group.objects)))
    for ob in obs:
        print("Rotation object {} with type {}".format(ob.name, ob.type))
        ob.rotation_euler = mathutils.Euler((0.0, 0.0, ob.rotation_euler.z + math.radians(rotation)), 'XYZ')


# reflection about plane perpendicular to the xy plane and at an angle given by rotation
def rotate_group_about_z(group, rotation=0.0):
    obs = [o for o in group.objects]
    print("Number of objects in the Input Group({}) = {}".format(group.name, len(group.objects)))
    for ob in obs:
        print("Rotation object {} with type {}".format(ob.name, ob.type))
        ob.rotation_euler = mathutils.Euler((0.0, 0.0, ob.rotation_euler.z + math.radians(rotation)), 'XYZ')