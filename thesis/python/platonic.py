#!/usr/bin/env python2.7
import bpy
from random import randint
import sys
from os.path import dirname
textFilePath = "./"
textFileFolder = dirname(textFilePath)  # = "/home/me/somewhere/textfiles"
sys.path.append(textFileFolder)

from look_at import look_at
from object_lowest_point import object_lowest_point
from mathutils import Vector

# To run:
# blender --background --engine CYCLES --python cubes.py -- --render="hello"


def render_cubes(save_path, render_path):
    scene = bpy.context.scene

    # Clear existing objects.
    scene.camera = None
    for obj in scene.objects:
        scene.objects.unlink(obj)

    # terahedron
    bpy.ops.mesh.primitive_cone_add(vertices=3, location=(-4.0, 0.0, 0.0))
    bpy.context.scene.objects.active.scale = (0.5, 0.5, 0.5)
    bpy.context.scene.objects.active.name = "Cone_Platonic"

    # cube
    bpy.ops.mesh.primitive_cube_add(location=(-2.0, 0.0, 0.0))
    bpy.context.scene.objects.active.scale = (0.5, 0.5, 0.5)
    bpy.context.scene.objects.active.name = "Cube_Platonic"

    # icosahedron
    bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=1, location=(-0.0, 0.0, 0.0))
    bpy.context.scene.objects.active.scale = (0.5, 0.5, 0.5)
    bpy.context.scene.objects.active.name = "Icosahedron_Platonic"


    scene.update()
    lowest_point = object_lowest_point(bpy.context.scene.objects['Cone_Platonic'])
    bpy.ops.mesh.primitive_plane_add(radius=100, location=(0, 0, lowest_point[2]))
    bpy.data.objects['Plane'].data.name = 'Ground Plane'
    ob = bpy.context.active_object
    # Get material
    mat = bpy.data.materials.get("Material")
    if mat is None:
        # create material
        mat = bpy.data.materials.new(name="Material")

    # Assign it to object
    if ob.data.materials:
        # assign to 1st material slot
        ob.data.materials[0] = mat
    else:
        # no slots
        ob.data.materials.append(mat)
    bpy.context.object.active_material.diffuse_color = (1, 1, 1)

    # Camera
    cam_data = bpy.data.cameras.new("MyCam")
    cam_ob = bpy.data.objects.new(name="MyCam", object_data=cam_data)
    scene.objects.link(cam_ob)  # instance the camera object in the scene
    scene.camera = cam_ob  # set the active camera
    cam_ob.location = (0.0, -15.0, 5.0)
    scene.update()
    origin = Vector((0.0, 0.0, 0.0))
    look_at(cam_ob, origin)

    # Lamp
    lamp_data = bpy.data.lamps.new("MyLamp", 'POINT')
    lamp_ob = bpy.data.objects.new(name="MyCam", object_data=lamp_data)
    scene.objects.link(lamp_ob)
    lamp_ob.location = 2.0, 2.0, 10.0
    lamp_ob.data.shadow_soft_size = 0.01
    lamp_ob.data.cycles.cast_shadow = True


    bpy.context.scene.world.light_settings.use_ambient_occlusion = True
    bpy.context.scene.cycles.samples = 50
    bpy.context.scene.cycles.sample_as_light = True
    bpy.context.scene.cycles.max_bounces = 12
    bpy.context.scene.cycles.min_bounces = 3
    bpy.context.scene.cycles.glossy_bounces = 0
    bpy.context.scene.cycles.sample_clamp_indirect = 3

    bpy.ops.wm.save_as_mainfile(filepath="debug.blend")

    if save_path:
        try:
            f = open(save_path, 'w')
            f.close()
            ok = True
        except:
            print("Cannot save to path %r" % save_path)

            import traceback
            traceback.print_exc()

        if ok:
            bpy.ops.wm.save_as_mainfile(filepath=save_path)

    if render_path:
        render = scene.render
        render.use_file_extension = True
        render.filepath = render_path
        bpy.ops.render.render(write_still=True)


def main():
    import sys  # to get command line args
    import argparse  # to parse options for us and print a nice help message

    # get the args passed to blender after "--", all of which are ignored by
    # blender so scripts may receive their own arguments
    argv = sys.argv

    if "--" not in argv:
        argv = []  # as if no args are passed
    else:
        argv = argv[argv.index("--") + 1:]  # get all args after "--"

    # When --help or no args are given, print this help
    usage_text = \
        "Run blender in background mode with this script:"
    "  blender --background --python " + __file__ + " -- [options]"

    parser = argparse.ArgumentParser(description=usage_text)

    # Example utility, add some text and renders or saves it (with options)
    # Possible types are: string, int, long, choice, float and complex.

    parser.add_argument("-r", "--render", dest="render_path", metavar='FILE',
                        help="Render an image to the specified path")

    parser.add_argument("-s", "--save", dest="save_path", metavar='FILE',
                        help="Save the generated file to the specified path")

    args = parser.parse_args(argv)  # In this example we wont use the args

    if not argv:
        parser.print_help()
        return

    if not args.render_path:
        print("Error: --render=\"path to render\" argument not given, aborting.")
        parser.print_help()
        return

    # Run the example function
    render_cubes(args.save_path, args.render_path)

    print("batch job finished, exiting")


if __name__ == "__main__":
    main()
