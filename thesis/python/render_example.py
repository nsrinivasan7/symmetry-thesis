import bpy
from mathutils import *
from math import *
#delete the cube
bpy.ops.object.delete(use_global=False)
# Add a camera fish eye.
bpy.context.scene.render.engine = 'CYCLES'
bpy.ops.object.camera_add(view_align=True, location=(0, 0, 2.5), rotation=(0, 0, 0))
bpy.context.object.rotation_euler[0] = 0
bpy.context.object.rotation_euler[1] = 0
bpy.context.object.rotation_euler[2] = 0
bpy.context.object.data.type = 'PANO'
bpy.context.object.data.cycles.fisheye_lens = 2.7
bpy.context.object.data.cycles.fisheye_fov = 3.14159
bpy.context.object.data.sensor_width = 8.8
bpy.context.object.data.sensor_height = 6.6
#Add a lamp.
bpy.ops.object.lamp_add(type='POINT', radius=1, view_align=False, location=(0, 1.5, 2.5),
layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
bpy.context.object.data.shadow_soft_size = 0.01
bpy.context.object.data.cycles.cast_shadow = True
#Import the model .mhx in the scene
bpy.ops.import_scene.makehuman_mhx(filepath="/Users/xxx/Documents/MakeHuman/v1/exports/scene1/person1.mhx")
#Set the rendering parameter
bpy.context.scene.render.resolution_x = 1360
bpy.context.scene.render.resolution_y = 1024
bpy.context.scene.render.resolution_percentage = 50
bpy.context.scene.frame_start = 1
bpy.context.scene.frame_end = 2
bpy.context.scene.frame_step = 1
bpy.context.scene.render.pixel_aspect_x = 1
bpy.context.scene.render.pixel_aspect_y = 1
bpy.context.scene.render.use_file_extension = True
bpy.context.scene.render.image_settings.color_mode ='RGBA'
bpy.context.scene.render.image_settings.file_format='PNG'
bpy.context.scene.render.filepath = "/Users/xx/Desktop/pythonscript/"
bpy.context.scene.render.image_settings.compression = 90
##sampling;=path tracing
bpy.context.scene.cycles.progressive = 'PATH'
bpy.context.scene.cycles.samples = 50
bpy.context.scene.cycles.max_bounces = 1
bpy.context.scene.cycles.min_bounces = 1
bpy.context.scene.cycles.glossy_bounces = 1
bpy.context.scene.cycles.transmission_bounces = 1
bpy.context.scene.cycles.volume_bounces = 1
bpy.context.scene.cycles.transparent_max_bounces = 1
bpy.context.scene.cycles.transparent_min_bounces = 1
bpy.context.scene.cycles.use_progressive_refine = True
bpy.context.scene.render.tile_x = 64
bpy.context.scene.render.tile_y = 64
#insert the walls
#1)the floor
bpy.ops.mesh.primitive_plane_add(radius=1, view_align=False, enter_editmode=False, location=(0, 0, 0), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
bpy.context.object.dimensions[0] = 4
bpy.context.object.dimensions[1] = 4
bpy.context.object.dimensions[2] = 0
#1)Wall 1
bpy.ops.mesh.primitive_cube_add(view_align=False, enter_editmode=False, location=(0, 2, 1.25), layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False))
bpy.context.object.dimensions[0] = 4
bpy.context.object.dimensions[1] = 0.05
bpy.context.object.dimensions[2] = 2.5
bpy.context.object.location[2] = 1.25
bpy.context.object.location[1] = 2

#Render results
bpy.ops.render.render(animation=True)