#!BPY

"""
Name: 'Create Spiral Stair'
Blender: 242
Group: 'Object'
Tooltip: 'creates a spiral staircase'
"""

__author__ = "ahmet öztürk"
__url__ = ("blender", "elysiun","my homepage, http://blhps.dizayn.de",)
__version__ = "0.6"
__bpydoc__ = """Description: creates a spiral staircase according to parameters.
Usage: all controls are self-explanatory.
Notes: currently, script only draws a circle instead of a central pole which, then, can be extruded to get the desired pole. more to come..
"""

# --------------------------------------------------------------------------
# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****
# --------------------------------------------------------------------------

import Blender
from Blender import Draw, BGL
import math
from math import *

def createStairCase():
	# INTERNAL VARIABLES
	treadDepth  = totAngle*radius/stepCount
	p = Blender.Window.GetCursorPos()


	# CENTER POLE
	if poleRadius != 0:
		pole = Blender.NMesh.GetRaw()

		v = Blender.NMesh.Vert(0.0,0.0,0.0)
		pole.verts.append(v)
		for n in range(8):
			v = Blender.NMesh.Vert(poleRadius*sin(n*2*pi/8), poleRadius*cos(n*2*pi/8), 0.0)
			pole.verts.append(v)

		for n in range (7):
			f = Blender.NMesh.Face()
			f.v.append(pole.verts[0])
			f.v.append(pole.verts[n+1])
			f.v.append(pole.verts[n+2])
			pole.faces.append(f)

		f = Blender.NMesh.Face()
		f.v.append(pole.verts[0])
		f.v.append(pole.verts[8])
		f.v.append(pole.verts[1])
		pole.faces.append(f)

		Blender.NMesh.PutRaw(pole, "staircase", 1)

		obj = Blender.Object.GetSelected()[0]
		obj.loc = p


	# TREADS
	tread = Blender.NMesh.GetRaw()

	v   = Blender.NMesh.Vert(0.0,0.0,0.0)
	tread.verts.append(v)
	v   = Blender.NMesh.Vert(radius,treadDepth/2,0.0)
	tread.verts.append(v)
	v   = Blender.NMesh.Vert(radius,-treadDepth/2,0.0)
	tread.verts.append(v)

	v   = Blender.NMesh.Vert(0.0,0.0,treadThickness)
	tread.verts.append(v)
	v   = Blender.NMesh.Vert(radius,treadDepth/2,treadThickness)
	tread.verts.append(v)
	v   = Blender.NMesh.Vert(radius,-treadDepth/2,treadThickness)
	tread.verts.append(v)

	# bottom face
	f = Blender.NMesh.Face()
	f.v.append(tread.verts[0])
	f.v.append(tread.verts[1])
	f.v.append(tread.verts[2])
	tread.faces.append(f)
	# top face
	f = Blender.NMesh.Face()
	f.v.append(tread.verts[3])
	f.v.append(tread.verts[4])
	f.v.append(tread.verts[5])
	tread.faces.append(f)
	#  face
	f = Blender.NMesh.Face()
	f.v.append(tread.verts[0])
	f.v.append(tread.verts[1])
	f.v.append(tread.verts[4])
	f.v.append(tread.verts[3])
	tread.faces.append(f)
	#  face
	f = Blender.NMesh.Face()
	f.v.append(tread.verts[1])
	f.v.append(tread.verts[2])
	f.v.append(tread.verts[5])
	f.v.append(tread.verts[4])
	tread.faces.append(f)
	#  face
	f = Blender.NMesh.Face()
	f.v.append(tread.verts[0])
	f.v.append(tread.verts[2])
	f.v.append(tread.verts[5])
	f.v.append(tread.verts[3])
	tread.faces.append(f)

	Blender.NMesh.PutRaw(tread, "tread.01", 1)
	# todo: center new ?


	obj = Blender.Object.GetSelected()[0]
	obj.loc = p


	for i in range(stepCount):
		Blender.Object.Duplicate()
		objlist = Blender.Object.GetSelected()

		obj = objlist[0]

		obj.LocZ += totHeight/stepCount
		obj.RotZ += totAngle/stepCount


	# HANDRAIL
	c  = Blender.Curve.New('handrail')
	sc = Blender.Scene.getCurrent()
	ob = Blender.Object.New('Curve')
	ob.link(c)
	sc.link(ob)


	for i in range(stepCount+2):
		pt = [radius*cos(i*totAngle/stepCount), radius*sin(i*totAngle/stepCount), i*totHeight/stepCount, 1.0]
		c.appendNurb(pt)
		c[0].append(pt)

	ob.loc = p
	ob.LocZ += sliderHNDRLHEIGHT.val


	c.setExt1(0)
	#c.setExt2(.05)
	c.setExt2(sliderHNDRLTHCKNS.val)
	c.setFlag(0)
	c.setBevresol(3)


	Blender.Redraw()


EVT_BTN_CRE	= 1
EVT_NUM_RSC	= 2
EVT_SLI_RSH	= 3
EVT_SLI_ANG	= 4
EVT_SLI_HND	= 5
EVT_SLI_RAD = 6
EVT_SLI_HNDTCK = 7
EVT_SLI_POLRAD	= 8
EVT_SLI_TRETCK	= 9
EVT_MNU_MTD	= 10


def event(evt, val):
	if evt == Draw.ESCKEY:
		Draw.Exit()
		return

def button_event(evt):
	global stepCount, totAngle, totHeight, radius, poleRadius, treadThickness
	if evt == EVT_BTN_CRE:
		totAngle = sliderANGLE.val / 180 * pi
		stepCount = numberSTEPCOUNT.val
		totHeight = sliderTOTALHEIGHT.val
		radius = sliderRADIUS.val
		poleRadius = sliderPOLERADIUS.val
		treadThickness = sliderTREADTHCKNS.val
		createStairCase()
	elif evt == EVT_NUM_RSC:
		stepCount = numberSTEPCOUNT.val
	elif evt == EVT_SLI_ANG:
		totAngle = sliderANGLE.val / 180 * pi
	elif evt == EVT_SLI_RSH:
		totHeight = sliderTOTALHEIGHT.val
	elif evt == EVT_SLI_RAD:
		radius = sliderRADIUS.val
	elif evt == EVT_SLI_HNDTCK:
		radius = sliderHNDRLTHCKNS.val
	#return

def draw_gui():
	global numberSTEPCOUNT, sliderANGLE, sliderTOTALHEIGHT, sliderHNDRLHEIGHT, sliderRADIUS, sliderHNDRLTHCKNS, sliderPOLERADIUS, sliderTREADTHCKNS
	Draw.PushButton ("CREATE!", EVT_BTN_CRE, 5, 5, 400, 25, "go!")

	sliderTREADTHCKNS = Draw.Slider ("Tread thickness: ", EVT_SLI_TRETCK, 5, 40, 400, 25, 0.05, 0.01, 0.5, 0, "tread thickness")
	sliderPOLERADIUS = Draw.Slider ("Pole radius: ", EVT_SLI_POLRAD, 5, 70, 400, 25, 0.3, 0.0, 1.0, 0, "central pole radius (0 for none)")
	sliderHNDRLTHCKNS = Draw.Slider ("Handrail thickness: ", EVT_SLI_HNDTCK, 5, 100, 400, 25, 0.03, 0.01, 0.1, 0, "handrail cross-sectional radius")
	sliderHNDRLHEIGHT = Draw.Slider ("Handrail height: ", EVT_SLI_HND, 5, 130, 400, 25, 0.9, 0.05, 1.5, 0, "handrail height")

	numberSTEPCOUNT = Draw.Number ("Step count:", EVT_NUM_RSC, 5, 170, 400, 25, 18, 0, 100, "number of steps")
	sliderANGLE = Draw.Slider ("Total angle: ", EVT_SLI_ANG, 5, 200, 400, 25, 270.0, 30.0, 360.0, 0, "total angle")
	sliderRADIUS = Draw.Slider ("Radius: ", EVT_SLI_RAD, 5, 230, 400, 25, 1.0, 0.5, 5.0, 0, "staricase radius")
	sliderTOTALHEIGHT = Draw.Slider ("Total height: ", EVT_SLI_RSH, 5, 260, 400, 25, 3.00, 0.01, 7.00, 0, "total height")

#	Draw.Menu ("Method: %t|Riser height %x1|Total height %x2", EVT_MNU_MTD, 5, 215, 400, 25, 1, "Entry method")


Draw.Register (draw_gui, event, button_event)