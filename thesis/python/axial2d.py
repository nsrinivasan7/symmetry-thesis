#!/usr/bin/env python2.7
import bpy
from random import randint
import sys
from os.path import dirname
textFilePath = "./"
textFileFolder = dirname(textFilePath)  # = "/home/me/somewhere/textfiles"
sys.path.append(textFileFolder)

from look_at import look_at
from object_lowest_point import object_lowest_point
from mathutils import Vector
import math

from move_group import duplicate_and_move_group, rotate_group_about_z
# To run:
# blender --background --engine CYCLES --python cubes.py -- --render="hello"

def xfrange(start, stop, step):
    i = 0
    while start + i * step < stop:
        yield start + i * step
        i += 1

def render_dihedral(text, save_path, render_path):
    scene = bpy.context.scene

    # Clear existing objects.
    scene.camera = None
    for obj in scene.objects:
        scene.objects.unlink(obj)

    # make a new group
    group = bpy.data.groups.new("dh_group")

    # create two materials
    mat_text = bpy.data.materials.new(name="Material_text")
    mat_octagon = bpy.data.materials.new(name="Material_oct")


    txt_data = bpy.data.curves.new(name="center_text", type='FONT')

    # Text Object
    txt_ob = bpy.data.objects.new(name="MyText", object_data=txt_data)
    scene.objects.link(txt_ob)  # add the data to the scene as an object
    txt_data.body = text        # the body text to the command line arg given
    txt_data.align = 'CENTER'   # center text
    txt_data.size = 0.5
    txt_ob.select = True
    # bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS')
    txt_ob.location += Vector((0, 0, 0.0001))
    # bpy.ops.transform.resize(value=(4, 4, 4))
    # Assign it to object
    if txt_ob.data.materials:
        # assign to 1st material slot
        txt_ob.data.materials[0] = mat_text
    else:
        # no slots
        txt_ob.data.materials.append(mat_text)
        txt_ob.active_material.diffuse_color = (1, 0, 0)
    group.objects.link(txt_ob)


    # draw an octagon
    bpy.ops.mesh.primitive_circle_add(vertices=8, fill_type='NGON', location=(0, 0, 0))
    bpy.context.scene.objects.active.name = "D1"

    scene.update()
    ob = bpy.context.active_object

    # Assign it to object
    if ob.data.materials:
        # assign to 1st material slot
        ob.data.materials[0] = mat_octagon
    else:
        # no slots
        ob.data.materials.append(mat_octagon)
    bpy.context.object.active_material.diffuse_color = (1, 1, 1)
    group.objects.link(ob)


    # Camera
    cam_data = bpy.data.cameras.new("MyCam")
    cam_ob = bpy.data.objects.new(name="MyCam", object_data=cam_data)
    scene.objects.link(cam_ob)  # instance the camera object in the scene
    scene.camera = cam_ob  # set the active camera
    cam_ob.location = (0.0, 0.0, 50.0)
    scene.update()

    # Lamp
    lamp_data = bpy.data.lamps.new("MyLamp", 'POINT')
    lamp_ob = bpy.data.objects.new(name="MyLamp", object_data=lamp_data)
    scene.objects.link(lamp_ob)
    lamp_ob.location = 2.0, 2.0, 10.0
    lamp_ob.data.shadow_soft_size = 0.01
    lamp_ob.data.cycles.cast_shadow = True

    # bpy.ops.wm.save_as_mainfile(filepath="debug.blend")
    allgroups = []
    allgroups.append(group)
    cur_group = group
    for rotation_z in xfrange(45, 360, 45):
        print(rotation_z)
        next_group = duplicate_and_move_group(cur_group, translation=(3.0, 0.0, 0.0))
        allgroups.append(next_group)
        cur_group = next_group
        rotate_group_about_z(next_group, rotation=45.0)

    bpy.context.scene.world.light_settings.use_ambient_occlusion = True
    bpy.context.scene.cycles.samples = 5
    bpy.context.scene.cycles.sample_as_light = True
    bpy.context.scene.cycles.max_bounces = 12
    bpy.context.scene.cycles.min_bounces = 3
    bpy.context.scene.cycles.glossy_bounces = 0
    bpy.context.scene.cycles.sample_clamp_indirect = 3

    if save_path:
        try:
            f = open(save_path, 'w')
            f.close()
            ok = True
        except:
            print("Cannot save to path %r" % save_path)

            import traceback
            traceback.print_exc()

        if ok:
            bpy.ops.wm.save_as_mainfile(filepath=save_path)

    if render_path:
        render = scene.render
        render.use_file_extension = True
        render.filepath = render_path
        bpy.ops.render.render(write_still=True)


def main():
    import sys  # to get command line args
    import argparse  # to parse options for us and print a nice help message

    # get the args passed to blender after "--", all of which are ignored by
    # blender so scripts may receive their own arguments
    argv = sys.argv

    if "--" not in argv:
        argv = []  # as if no args are passed
    else:
        argv = argv[argv.index("--") + 1:]  # get all args after "--"

    # When --help or no args are given, print this help
    usage_text = \
        "Run blender in background mode with this script:"
    "  blender --background --python " + __file__ + " -- [options]"

    parser = argparse.ArgumentParser(description=usage_text)

    # Example utility, add some text and renders or saves it (with options)
    # Possible types are: string, int, long, choice, float and complex.
    parser.add_argument("-t", "--text", dest="text", type=str, required=True,
            help="This text will be used to render an image")

    parser.add_argument("-r", "--render", dest="render_path", metavar='FILE',
                        help="Render an image to the specified path")

    parser.add_argument("-s", "--save", dest="save_path", metavar='FILE',
                        help="Save the generated file to the specified path")

    args = parser.parse_args(argv)  # In this example we wont use the args

    if not argv:
        parser.print_help()
        return

    if not args.text:
        print("Error: --text=\"text\" argument not given, aborting.")
        parser.print_help()
        return

    # Run the example function
    render_dihedral(args.text, args.save_path, args.render_path)

    print("batch job finished, exiting")


if __name__ == "__main__":
    main()
