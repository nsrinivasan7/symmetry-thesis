%!TEX root = supplementalMaterial.tex

\renewcommand{\theequation}{A.\arabic{equation}}

\section{Jacobians of the cost}
\label{sec:supJac}

\newcommand{\jac}[2]{\frac{\partial{#1}}{\partial{#2}}}

Our constrained bundle adjustment approach requires to minimize a cost (eq.~\eqref{eq:ourBA} in the main paper), 
involving the 3D points, the camera parameters, and the generators.
This optimization is done using the Levenberg-Marquardt method, hence it requires 
linearizing the residual errors in the cost, at each iteration. 
In this appendix, we give analytic expressions for the Jacobian matrices 
required for linearization, omitting basic derivatives arising in standard \BA. 

We start by recalling the cost function from eq.~\eqref{eq:ourBA} in the main paper.
%
\beq
\label{eq:ourBAsupp}
\min_{ \{\point_\ptInd\}, \{\camera_\camInd\}, \{t_\genInd\}, \{R_\genInd\} } 
E_{\BA} \left(\{\point_\ptInd\}, \{\camera_\camInd\} \right)
 + 
 E_t\left(\{\point_\ptInd\}, \{\camera_\camInd\}, \{t_\genInd\} \right) + 
 E_R\left(\{\point_\ptInd\}, \{\camera_\camInd\}, \{R_\genInd\} \right)
\eeq
%
where:

\vspace{-1cm}
\bea
E_{\BA}(\{\point_\ptInd\}, \{\camera_\camInd\}) &=& 
\sum_{\measurement\pairInd \in \MeasSet} 
f( \pi(\camera_\camInd,\point_\ptInd) - \measurement\pairInd)
\\
E_t\left(\{\point_\ptInd\}, \{\camera_\camInd\}, \{t_\genInd\} \right) &=&  
\sum_{
\substack{ (\point_\ptInd,\point_\ptIndd,t_\genInd,n) \in \symSet_t }
}  \;\;
\sum_{\camInd \in \calC_\ptIndd} \;\;
f( \pi(\camera_\camInd, \point_\ptInd + n t_\genInd) - \measurement\pairIndd) )
\\
E_R\left(\{\point_\ptInd\}, \{\camera_\camInd\}, \{R_\genInd\} \right) &=&  
\sum_{
\substack{ (\point_\ptInd,\point_\ptIndd,R_\genInd,n) \in \symSet_R }
}  \;\;
\sum_{\camInd \in \calC_\ptIndd} \;\;
f( \pi(\camera_\camInd, R_\genInd^{n} \point_\ptInd ) - \measurement\pairIndd) )
\eea
%
The residual errors for which we need to compute 
%Since differentiation is a linear operator, we only need to compute derivatives for the 
%following residual errors 
a Jacobian matrix are then:
%
\bea
\label{eq:residualErrors}
r^{\BA}%(\{\point_\ptInd\}, \{\camera_\camInd\}) 
&\doteq& 
\pi(\camera_\camInd,\point_\ptInd) - \measurement\pairInd
\\
r^t%\left(\{\point_\ptInd\}, \{\camera_\camInd\}, \{t_\genInd\} \right) 
&\doteq&  
\pi(\camera_\camInd, \point_\ptInd + n t_\genInd) - \measurement\pairIndd
\\
r^R%\left(\{\point_\ptInd\}, \{\camera_\camInd\}, \{R_\genInd\} \right)
&\doteq& 
\pi(\camera_\camInd, R_\genInd^{n} \point_\ptInd ) - \measurement\pairIndd
\eea
%
%where we omitted the indices for simplicity.
We do not repeat the derivation of following Jacobians, that arise in standard \BA:
%
\bea
\label{eq:usualJacobians}
 \jac{r^{\BA}}{\camera_\camInd} = \jac{\pi}{\camera_\camInd} \doteq J_{\pi\camera}
 \hspace{1cm}
 \jac{r^{\BA}}{\point_\ptInd} = \jac{\pi}{\point_\ptInd} \doteq J_{\pi\point}
\eea
%  
Using the chain rule and the definitions in~\eqref{eq:usualJacobians}, 
it is easy to compute the Jacobians of  $r^t$:
%
\bea
\label{eq:rt}
 \displaystyle \jac{r^t}{\camera_\camInd} 
 &=&  \displaystyle  
 \jac{\pi}{\camera_\camInd} = J_{\pi\camera} 
 \\
  \displaystyle  \jac{r^t}{\point_\ptInd} 
   &=&  \displaystyle 
   \jac{r^t}{(\point_\ptInd + n t_\genInd)} \; \jac{(\point_\ptInd + n t_\genInd)}{\point_\ptInd}
  = J_{\pi\point}
   \\
  \displaystyle  \jac{r^t}{t_\genInd} 
   &=&  \displaystyle 
   \jac{r^t}{(\point_\ptInd + n t_\genInd)} \; \jac{(\point_\ptInd + n t_\genInd)}{t_\genInd}
  = n \; J_{\pi\point}   
\eea
%
Similarly, some of the Jacobians of $r^R$ are straightforward to compute:
%
\bea
\label{eq:rt}
 \displaystyle \jac{r^R}{\camera_\camInd} 
 &=&  \displaystyle  
 \jac{\pi}{\camera_\camInd} = J_{\pi\camera} 
 \\
  \displaystyle  \jac{r^t}{\point_\ptInd} 
   &=&  \displaystyle 
   \jac{r^R}{(R_\genInd^{n} \point_\ptInd)} \; \jac{(R_\genInd^{n} \point_\ptInd)}{\point_\ptInd}
  = J_{\pi\point} \; R_\genInd^{n}
\eea
%

The Jacobian of $r^R$ with respect to the rotation generator $R$ is slightly more complicated to compute: this is due to the 
fact that $R$ lives on the manifold \SOthree, rather than in a vector space.
 We follow standard approaches for optimization on manifold~\cite{Absil07book} 
 and rewrite the matrix $R$ as $R(\theta_\genInd) \doteq \hat{R}\;\expmap{\theta_k}$, where $\hat{R}$ is a fixed 
 rotation corresponding to the linearization point, $\theta_\genInd \in \Real{3}$ is a small perturbation, and $\expmap{\cdot}$ is the exponential map for \SOthree. 
 %The \emph{retraction} $R(\theta)$ is simply a map between a perturbation in the 
 %tangent space at $\hat{R}$ ($\theta$) and a perturbation on the manifold. 
%If we define $r^R = $
 Substituting $R(\theta_\genInd)$ in 
 the expression of  $r^R$ in~\eqref{eq:residualErrors} we get:
%
\bea
r^R%\left(\{\point_\ptInd\}, \{\camera_\camInd\}, \{R_\genInd\} \right)
&\doteq& 
\pi(\camera_\camInd, R(\theta_\genInd)^{n} \point_\ptInd ) - \measurement\pairIndd 
%=
%\pi(\camera_\camInd, R(\theta_\genInd)^{n} \point_\ptInd ) - \measurement\pairIndd
\eea
%
This reparametrization is usually called ``lifting''~\cite{Absil07fcm}, while $R(\theta_\genInd)$ 
is called a \emph{retraction}. 

Using the chain rule we get:
%
\bea
\label{eq:function0}
 \displaystyle \jac{r^R}{\theta_\genInd} 
 &=&  \displaystyle  
 \jac{r^R}{(R(\theta_\genInd)^{n} \point_\ptInd)}  \; \jac{(R(\theta_\genInd)^{n} \point_\ptInd)}{\theta_\genInd} 
 = J_{\pi\point} \;\jac{(R(\theta_\genInd)^{n} \point_\ptInd)}{\theta_\genInd} 
\eea
% 
Now the only remaining step is to compute $\jac{(R(\theta_\genInd)^{n} \point_\ptInd)}{\theta_\genInd}$.
To compute this Jacobian, we write a first-order Taylor expansion of the function $R(\theta_\genInd)^{n} \point_\ptInd$, 
from which is easy to read the Jacobian. %, which appears as coefficient matrix of the first-order terms in $\theta_\genInd$.
The function $R(\theta_\genInd)^{n} \point_\ptInd$ includes $n$ products of rotation matrices:
%
\bea
\label{eq:function1}
R(\theta_\genInd)^{n} \point_\ptInd 
&=& (\hat{R} \; \expmap{\theta_\genInd}) \; (\hat{R} \; \expmap{\theta_\genInd}) \; \ldots
 \; (\hat{R} \; \expmap{\theta_\genInd}) \point_\ptInd 
\eea
%
In order to write the first-order Taylor expansion of~\eqref{eq:function1}, we use
the first-order approximation of the exponential map $\expmap{\theta_\genInd} \approx I + S(\theta_\genInd)$, 
where $I$ is the $3\times 3$ identity matrix and $S(\theta_\genInd)$ is a skew-symmetric matrix with entries 
specified by the vector $\theta_\genInd$. Substituting this first-order approximation in~\eqref{eq:function1} we get:  
%
\bea
R(\theta_\genInd)^{n} \point_\ptInd 
 &\overset{\text{1st order}}{\approx}& 
  (\hat{R} + \hat{R} S(\theta_\genInd)) \; (\hat{R} + \hat{R} S(\theta_\genInd)) \; \ldots
 \; (\hat{R} + \hat{R} S(\theta_\genInd)) \point_\ptInd \\
 &\overset{\text{1st order}}{\approx}& 
 \left(\hat{R}^2 + \hat{R} S(\theta_\genInd) \hat{R} + \hat{R}^2 S(\theta_\genInd) \right) \; \ldots
 \; (\hat{R} + \hat{R} S(\theta_\genInd)) \point_\ptInd \\
 % &\overset{\text{1st order}}{\approx}& 
 % \left(\hat{R}^3 + \hat{R} S(\theta_\genInd) \hat{R}^2 + \hat{R}^2 S(\theta_\genInd) \hat{R} 
 % +\hat{R}^3 S(\theta_\genInd) \right) \; \ldots
 % \; (\hat{R} + \hat{R} S(\theta_\genInd)) \point_\ptInd \\
 &\overset{\text{1st order}}{\approx}& 
 \left( \hat{R}^n + \sum_{i=1}^n 
  \hat{R}^i S(\theta_\genInd) \hat{R}^{n-i}
 \right) \point_\ptInd \label{eq:function2}
\eea
%
Now we use the following property of skew-symmetric matrices:
 %(i) for any $a\in\Real{3}$ and any rotation $R$, it holds $S(a) R = 
 %R \; R\tran \; S(a) \; R = R \; S(R\tran a)$, and (ii) 
 for any $a,b\in\Real{3}$, it holds $S(a) b = -S(b) a$, which allows 
 rewriting~\eqref{eq:function2} as:
%
\bea
R(\theta_\genInd)^{n} \point_\ptInd 
 &\overset{\text{1st order}}{\approx}& 
 \hat{R}^n \point_\ptInd + \sum_{i=1}^n 
  \hat{R}^i S(\theta_\genInd) \hat{R}^{n-i}
\point_\ptInd 
%   &\overset{\text{using (i)}}{=}&
%   \hat{R}^n \point_\ptInd + \sum_{i=1}^n 
% \hat{R}^i \hat{R}^{n-i} S( (\hat{R}^{n-i})\tran \theta_\genInd) 
% \point_\ptInd \\
=
  \hat{R}^n \point_\ptInd - \sum_{i=1}^n 
\hat{R}^i S( \hat{R}^{n-i}
\point_\ptInd )  \theta_\genInd \label{eq:function3}
\eea
%
We are now able to read the Jacobian $\jac{(R(\theta_\genInd)^{n} \point_\ptInd)}{\theta_\genInd}$ directly from eq.~\eqref{eq:function3}: 
the Jacobian is simply the coefficient matrix of the first-order terms in $\theta_\genInd$, hence:
%
\bea
\label{eq:function4}
 \displaystyle \jac{R(\theta_\genInd)^{n} \point_\ptInd}{\theta_\genInd} 
 &=&  \displaystyle  
- \sum_{i=1}^n 
\hat{R}^i S( \hat{R}^{n-i}
\point_\ptInd )  
\eea
% 
Substituting~\eqref{eq:function4} into~\eqref{eq:function0} we obtain the desired Jacobian:
%
\bea
 \displaystyle \jac{r^R}{\theta_\genInd} 
 &=&  \displaystyle  
 - J_{\pi\point} \; \sum_{i=1}^n 
\hat{R}^i S( \hat{R}^{n-i}
\point_\ptInd ). 
\eea
%
As an implementation remark we note that the product $\hat{R}^i$
can be conveniently computed as $\hat{R}^i = \expmap{i \hat\theta}$, where 
$\hat\theta \in \Real{3}$ are the exponential coordinates of $\hat{R}$ 
(roughly speaking, if $(u,\phi)$ is the axis-angle representation of $\hat{R}$, 
then $\hat\theta = u \phi$), see, e.g.,~\cite{Peters14sicon}.