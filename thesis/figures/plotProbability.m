

C = [1.59 4.34;235 240.285; 495 469];
C = [1.59/(1.59+4.34) 4.34/(1.59+4.34); 235/(235+240) 240/(235+240); 495/(495+469) 469/(495+469)];
%C = [0.8 0.25; 0.65 0.45; 0.05 0.95];
b = bar(C);
set(gca,'XTickLabel',{'Config#1', 'Config#2', 'Config#3'})
ylabel('Negative Log Likelihood');
title('Normalized Negative Log Likelihood');
legend('Correct (g,p_1(j))' ,'Incorrect (g,p_1(j))');
b(2).FaceColor = [91.0/255.0, 155.0/255.0,213.0/255.0];
b(1).FaceColor = [191.0/255.0, 144.0/255.0,0/255.0];