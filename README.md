# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Thesis of Natesh Srinivasan

### How do I get set up? ###

* Render lyx file or view PDF
* http://www.thousandmonkeys.com/build-master-document-from-child-in-lyx/
* http://www.aarondefazio.com/tangentially/?p=19
* you need biber on linux : sudo apt-get install texlive-bibtex-extra biber
* then do a lyx reconfigure
* change path in general-preable to the location of the "thesis" folder. This takes only absolute path
* Tools > Preferences > Output > LaTeX > Bibliography generation > Processor > biber
* Document > Settings > Bibliography to biber

### Contribution guidelines ###

* Figures go into figures. Naming convention is ChapterName_Imagename.extension

### Who do I talk to? ###

* Natesh Srinivasan (natesh.rvce@gmail.com)